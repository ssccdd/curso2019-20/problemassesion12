/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion12.grupo4;

import es.uja.ssccdd.curso1920.problemassesion12.grupo4.Constantes.TipoCasa;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 *
 * @author pedroj
 */
public class SensorListener implements MessageListener {
    private final String consumerName;
    private final List<Casa> casasPromotor;

    public SensorListener(String consumerName, List<Casa> casasPromotor) {
        this.consumerName = consumerName;
        this.casasPromotor = casasPromotor;
    }

    @Override
    public void onMessage(Message msg) {
        GsonUtil<Sensor> gsonUtil = new GsonUtil();
        
        try {			
            if (msg instanceof TextMessage) {		
                TextMessage contenido = (TextMessage) msg;
                Sensor sensor = gsonUtil.decode(contenido.getText(), Sensor.class);
		System.out.println(consumerName + " processing job: " + sensor);
                instalarSensor(sensor);
            } else
                System.out.println(consumerName + " Unknown message");
	} catch (JMSException | InterruptedException ex) {			
            Logger.getLogger(SensorListener.class.getName()).log(Level.SEVERE, null, ex);
	}
    }
    
    
    private void instalarSensor(Sensor sensor) throws InterruptedException {
        Casa casa;
        Iterator it = casasPromotor.iterator();
        boolean asignado = false;
        
        // Instalar el sensor en una casa si es posible
        while ( it.hasNext() && !asignado ) {
            casa = (Casa) it.next();
            asignado = casa.addSensor(sensor);
        }
        
        // Si no se ha podido instalar el sensor se crea una nueva casa
        // aleatoria donde se instala el sensor y se añade a la lista
        if( !asignado ) {
            casa = new Casa(casasPromotor.size(),TipoCasa.getCasa());
            casa.addSensor(sensor);
            casasPromotor.add(casa);
        }
        
        // Se simula el tiempo de instalación del sensor
        System.out.println("Tarea-" + consumerName + " instala el sensor " + sensor);
        TimeUnit.SECONDS.sleep(sensor.getTiempoInstalacion());
    }
}
