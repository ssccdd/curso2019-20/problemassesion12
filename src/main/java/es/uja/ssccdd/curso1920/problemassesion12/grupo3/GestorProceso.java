/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion12.grupo3;

import static es.uja.ssccdd.curso1920.problemassesion12.grupo3.Constantes.BROKER_URL;
import static es.uja.ssccdd.curso1920.problemassesion12.grupo3.Constantes.QUEUE;
import static es.uja.ssccdd.curso1920.problemassesion12.grupo3.Constantes.TIEMPO_GESTION;
import es.uja.ssccdd.curso1920.problemassesion12.grupo3.Constantes.TipoProceso;
import java.util.concurrent.TimeUnit;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;

/**
 *
 * @author pedroj
 */
public class GestorProceso implements Runnable {
    private final String iD;
    private final int numProcesos;
    private ActiveMQConnectionFactory connectionFactory;
    private Connection connection;
    private Session session;
    private Destination destination;

    public GestorProceso(String iD, int numProcesos) {
        this.iD = iD;
        this.numProcesos = numProcesos;
    }

    @Override
    public void run() {
        System.out.println("TAREA-" + iD + " Comienza su ejecución...");
        
        try {
            inicio();
            
            // Creamos los procesos 
            for(int i = 0; i < numProcesos; i++)
                crearProceso(i);
        } catch (Exception e) {
            System.out.println("TAREA-" + iD + 
                               " Hay una INCIDENCIA en la ejecución: " + e.getMessage());
        } finally {
            fin();
            System.out.println("TAREA-" + iD + " Finaliza su ejecución...");
        }
    }
    
    private void inicio() throws Exception {
        connectionFactory = new ActiveMQConnectionFactory(BROKER_URL);
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue(QUEUE);
    }
    
    private void crearProceso(int indice) throws Exception {
        GsonUtil<Proceso> gsonUtil = new GsonUtil();
        
        if( Thread.interrupted() )
            throw new InterruptedException();
        
        // Se genera un proceso aleatorio simulando tiempo de gestión
        TipoProceso tipoProceso = TipoProceso.getProceso();
        Proceso proceso = new Proceso(iD.hashCode()+indice,tipoProceso);
        TimeUnit.SECONDS.sleep(TIEMPO_GESTION);
        
        // Se genera el mensaje asociado al proceso para su envío al destino
        MessageProducer producer = session.createProducer(destination);
        TextMessage message = session.createTextMessage(gsonUtil.encode(proceso, Proceso.class));
        producer.send(message);
        producer.close();
        
        System.out.println("TAREA-" + iD + " ha creado el " + proceso + 
                           " total procesos creados " + (indice+1));
    }
    
    private void fin() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (Exception ex) {
            // No hacer nada
        }
    }
}
