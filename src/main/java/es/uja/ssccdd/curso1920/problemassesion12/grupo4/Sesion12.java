/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion12.grupo4;

import static es.uja.ssccdd.curso1920.problemassesion12.grupo4.Constantes.TIEMPO_ESPERA;
import static es.uja.ssccdd.curso1920.problemassesion12.grupo4.Constantes.NUM_SENSORES;
import static es.uja.ssccdd.curso1920.problemassesion12.grupo4.Constantes.PROMOTORES;
import static es.uja.ssccdd.curso1920.problemassesion12.grupo4.Constantes.SENSORES;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Sesion12 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        ExecutorService ejecucion;
        List<Future<?>> listaTareas;
        Future<?> tarea;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicialización de las variables para la prueba
        ejecucion = Executors.newCachedThreadPool();
        listaTareas = new ArrayList();
        
        // Se crean y se ejecutan las tareas fabricantes de sensores
        for(int i = 0; i < SENSORES.length; i++) {
            Fabricante fabricante;
            fabricante = new Fabricante(SENSORES[i].name(), SENSORES[i], NUM_SENSORES);
            tarea = ejecucion.submit(fabricante);
            listaTareas.add(tarea);
        }
        
        // Se crean y se ejecutan las tareas promotor para la instalación de sus casas
        for(int i = 0; i < PROMOTORES; i++) {
            Promotor promotor;
            promotor = new Promotor("Promotor-"+i);
            tarea = ejecucion.submit(promotor);
            listaTareas.add(tarea);
        }
        
        // Esperamos por un tiempo
        System.out.println("HILO(Principal) SUSPENDIDO POR UN TIEMPO");
        TimeUnit.MINUTES.sleep(TIEMPO_ESPERA);
        
        // Se solicita la cancelación de los pedidos que han excedido el tiempo
        System.out.println("HILO(Principal) Solicita la finalización de los pedidos");
        for( Future<?> task : listaTareas ) 
            task.cancel(true);
        
        // Finalizamos el ejecutor y esperamos a que todas las tareas finalicen
        System.out.println("HILO(Principal) Espera a la finalización de las tareas");
        ejecucion.shutdown();
        ejecucion.awaitTermination(TIEMPO_ESPERA, TimeUnit.DAYS);
        
        // Finalización del hilo principal
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
}
