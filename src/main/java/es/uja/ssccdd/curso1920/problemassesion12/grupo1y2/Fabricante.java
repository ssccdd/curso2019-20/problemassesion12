/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion12.grupo1y2;

import static es.uja.ssccdd.curso1920.problemassesion12.grupo1y2.Constantes.BROKER_URL;
import static es.uja.ssccdd.curso1920.problemassesion12.grupo1y2.Constantes.QUEUE;
import es.uja.ssccdd.curso1920.problemassesion12.grupo1y2.Constantes.TipoComponente;
import java.util.concurrent.TimeUnit;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;


/**
 *
 * @author pedroj
 */
public class Fabricante implements Runnable {
    private final String iD;
    private final TipoComponente tipoComponente;
    private final int unidades;
    private ActiveMQConnectionFactory connectionFactory;
    private Connection connection;
    private Session session;
    private Destination destination;

    public Fabricante(String iD, TipoComponente tipoComponente, int unidades) {
        this.iD = iD;
        this.tipoComponente = tipoComponente;
        this.unidades = unidades;
    }

    public String getiD() {
        return iD;
    }

    @Override
    public void run() {
        System.out.println("TAREA-" + iD + " Comienza su producción...");
        
        try {
            inicio();
            produccion();
        } catch (Exception e) {
            System.out.println("TAREA-" + iD + 
                               " Hay una INCIDENCIA en la producción: " + e.getMessage());
        } finally {
            fin();
            System.out.println("TAREA-" + iD + " Finaliza su producción...");
        }
    }
    
    private void inicio() throws Exception {
        connectionFactory = new ActiveMQConnectionFactory(BROKER_URL);
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue(QUEUE);
    }
    
    private void produccion() throws Exception {
        GsonUtil<Componente> gsonUtil = new GsonUtil();
        
        // Se producen los componentes del ordenador
        for(int i = 0; i < unidades; i++) {
            MessageProducer producer = session.createProducer(destination);
            Componente componente = new Componente(iD+"-"+i,tipoComponente);
            TextMessage msg = session.createTextMessage();
            msg.setText(gsonUtil.encode(componente, Componente.class));
            
            // Se envía el mensaje
            producer.send(msg);
            producer.close();
            
            // Se simula el tiempo de fabricación 
            TimeUnit.SECONDS.sleep(componente.tiempoFabricacion());
            System.out.println("TAREA-" + iD + " ha producido un componente " + componente);
        }
    }
    
    private void fin() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (Exception ex) {
            // No hacer nada
        }
    }
}
