/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion12.grupo4;

import static es.uja.ssccdd.curso1920.problemassesion12.grupo4.Constantes.BROKER_URL;
import static es.uja.ssccdd.curso1920.problemassesion12.grupo4.Constantes.QUEUE;
import es.uja.ssccdd.curso1920.problemassesion12.grupo4.Constantes.TipoSensor;
import java.util.concurrent.TimeUnit;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;

/**
 *
 * @author pedroj
 */
public class Fabricante implements Runnable {
    private final String iD;
    private final TipoSensor tipoSensor;
    private final int unidades;
    private ActiveMQConnectionFactory connectionFactory;
    private Connection connection;
    private Session session;
    private Destination destination;

    public Fabricante(String iD, TipoSensor tipoSensor, int unidades) {
        this.iD = iD;
        this.tipoSensor = tipoSensor;
        this.unidades = unidades;
    }

    @Override
    public void run() {
        System.out.println("TAREA-" + iD + " Comienza su producción...");
        
        try {
            inicio();
            produccion();
        } catch (Exception e) {
            System.out.println("TAREA-" + iD + 
                               " Hay una INCIDENCIA en la producción: " + e.getMessage());
        } finally {
            fin();
            System.out.println("TAREA-" + iD + " Finaliza su producción...");
        }
    }
    
    private void inicio() throws Exception {
        connectionFactory = new ActiveMQConnectionFactory(BROKER_URL);
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue(QUEUE);
    }
    
    private void produccion() throws Exception {
        GsonUtil<Sensor> gsonUtil = new GsonUtil();
        
        // Se producen los sensores para las casas
        for(int i = 0; i < unidades; i++) {
            MessageProducer producer = session.createProducer(destination);
            Sensor sensor = new Sensor(iD.hashCode()+i,tipoSensor);
            TextMessage message = session.createTextMessage(gsonUtil.encode(sensor, Sensor.class));
            
            // Se envía el mensaje
            producer.send(message);
            producer.close();
            
            // Se simula el tiempo de fabricación 
            TimeUnit.SECONDS.sleep(TipoSensor.tiempoFabricacion());
            System.out.println("TAREA-" + iD + " ha producido un sensor " + sensor);
        }
    }
    
    private void fin() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (Exception ex) {
            // No hacer nada
        }
    }
}
