/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion12.grupo1y2;

import static es.uja.ssccdd.curso1920.problemassesion12.grupo1y2.Constantes.BROKER_URL;
import static es.uja.ssccdd.curso1920.problemassesion12.grupo1y2.Constantes.QUEUE;
import static es.uja.ssccdd.curso1920.problemassesion12.grupo1y2.Constantes.TIEMPO_PROVEEDOR;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import org.apache.activemq.ActiveMQConnectionFactory;

/**
 *
 * @author pedroj
 */
public class Proveedor implements Runnable {
    private final String iD;
    private final ArrayList<Ordenador> pedido;
    private ActiveMQConnectionFactory connectionFactory;
    private Connection connection;
    private Session session;
    private Destination destination;

    public Proveedor(String iD) {
        this.iD = iD;
        this.pedido = new ArrayList();
    }

    @Override
    public void run() {
        System.out.println("TAREA-" + iD + " Comienza con su pedido...");
        
        try {
            inicio();
            
            prepararPedido();
        } catch (Exception e) {
            System.out.println("TAREA-" + iD + 
                               " Hay una INCIDENCIA en la confección del pedido: " + e.getMessage());
        } finally {
            fin();
            presentarPedido();
            System.out.println("TAREA-" + iD + " Finaliza su ejecución...");
        }   
    }

    public String getiD() {
        return iD;
    }
    
    private void inicio() throws Exception {
        connectionFactory = new ActiveMQConnectionFactory(BROKER_URL);
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue(QUEUE);
    }
    
    private void prepararPedido() throws Exception {
        MessageConsumer consumer = session.createConsumer(destination);
        
        consumer.setMessageListener(new ComponenteListener(iD, pedido));
        
        // Espera antes de finalizar
        TimeUnit.MINUTES.sleep(TIEMPO_PROVEEDOR);
        
        consumer.close();
    }
    
    private void fin() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (Exception ex) {
            // No hacer nada
        }
    }
    
    private void presentarPedido() {
        // Presentamos el resultado del pedido
        System.out.println("TAREA-" + iD + " el pedido obtenido es \n______________");
        for( Ordenador ordenador : pedido )
            System.out.println(ordenador.toString());
    }
}
