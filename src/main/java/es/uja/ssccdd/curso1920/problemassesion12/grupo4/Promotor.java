/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion12.grupo4;

import static es.uja.ssccdd.curso1920.problemassesion12.grupo4.Constantes.BROKER_URL;
import static es.uja.ssccdd.curso1920.problemassesion12.grupo4.Constantes.QUEUE;
import static es.uja.ssccdd.curso1920.problemassesion12.grupo4.Constantes.TIEMPO_INSTALACION;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import org.apache.activemq.ActiveMQConnectionFactory;

/**
 *
 * @author pedroj
 */
public class Promotor implements Runnable {
    private final String iD;
    private final ArrayList<Casa> instalacion;
    private ActiveMQConnectionFactory connectionFactory;
    private Connection connection;
    private Session session;
    private Destination destination;

    public Promotor(String iD) {
        this.iD = iD;
        this.instalacion = new ArrayList();
    }

    @Override
    public void run() {
        System.out.println("TAREA-" + iD + " Comienza con su instalación...");
        
        try {
            inicio();
            
            realizarInstalacion();
        } catch (Exception e) {
            System.out.println("TAREA-" + iD + 
                               " Hay una INCIDENCIA en la instalación: " + e.getMessage());
        } finally {
            fin();
            presentarInstalacion();
            System.out.println("TAREA-" + iD + " Finaliza su ejecución...");
        }   
    }
    
    private void inicio() throws Exception {
        connectionFactory = new ActiveMQConnectionFactory(BROKER_URL);
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination =  session.createQueue(QUEUE);
    }
    
    private void realizarInstalacion() throws Exception {
        MessageConsumer consumer = session.createConsumer(destination);
        
        consumer.setMessageListener(new SensorListener(iD, instalacion));
        
        // Espera antes de finalizar
        TimeUnit.MINUTES.sleep(TIEMPO_INSTALACION);
        
        consumer.close();
    }
    
    private void fin() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (Exception ex) {
            // No hacer nada
        }
    }
    
    private void presentarInstalacion() {
        // Presentamos el resultado del pedido
        System.out.println("TAREA-" + iD + " las casas instaladas son\n______________");
        for( Casa casa : instalacion )
            System.out.println(casa.toString());
    }
}
