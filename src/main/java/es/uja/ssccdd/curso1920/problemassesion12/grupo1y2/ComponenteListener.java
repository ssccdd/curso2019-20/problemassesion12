/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion12.grupo1y2;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 *
 * @author pedroj
 */
public class ComponenteListener implements MessageListener {
    private final String consumerName;
    private final List<Ordenador> pedido;

    public ComponenteListener(String consumerName, List<Ordenador> pedido) {
        this.consumerName = consumerName;
        this.pedido = pedido;
    }

    @Override
    public void onMessage(Message msg) {
        GsonUtil<Componente> gsonUtil = new GsonUtil();
        
        try {			
            if (msg instanceof TextMessage) {		
                TextMessage contenido = (TextMessage) msg;
                Componente componente = gsonUtil.decode(contenido.getText(), Componente.class);
		System.out.println(consumerName + " processing job: " + componente);
                addComponente(componente);
            } else
                System.out.println(consumerName + " Unknown message");
	} catch (JMSException | InterruptedException ex) {			
            Logger.getLogger(ComponenteListener.class.getName()).log(Level.SEVERE, null, ex);
	}
    }
    
    private void addComponente(Componente componente) throws InterruptedException {
        Ordenador ordenador;
        Iterator it = pedido.iterator();
        boolean asignado = false;
        
        // Asigna componente a un ordenador si es posible
        while ( it.hasNext() && !asignado ) {
            ordenador = (Ordenador) it.next();
            asignado = ordenador.addComponente(componente.getComponente());
            
            // Simulamos el tiempo de montaje si el ordenador está completo
            if( ordenador.ordenadorCompleto() ) {
                System.out.println(consumerName + " ha completado el ordenador " + ordenador);
                TimeUnit.SECONDS.sleep(ordenador.tiempoMontaje());
            }
        }
        
        // Si no se ha podido asignar el componente se crea un nuevo ordenador
        // al pedido y se le asigna el componente
        if( !asignado ) {
            ordenador = new Ordenador();
            ordenador.setiD("Proveedor("+pedido.size()+")");
            ordenador.addComponente(componente.getComponente());
            pedido.add(ordenador);
        }
    }
}
