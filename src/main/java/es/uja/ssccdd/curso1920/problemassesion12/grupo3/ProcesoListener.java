/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion12.grupo3;

import static es.uja.ssccdd.curso1920.problemassesion12.grupo3.Constantes.EstadoEjecucion.EN_EJECUCION;
import static es.uja.ssccdd.curso1920.problemassesion12.grupo3.Constantes.EstadoEjecucion.FINALIZADO;
import java.util.concurrent.TimeUnit;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author pedroj
 */
public class ProcesoListener implements MessageListener {
    private final String consumerName;

    public ProcesoListener(String consumerName) {
        this.consumerName = consumerName;
    }
    
    @Override
    public void onMessage(Message msg) {
        GsonUtil<Proceso> gsonUtil = new GsonUtil();
        
        try {			
            if (msg instanceof TextMessage) {		
                TextMessage contenido = (TextMessage) msg;
                Proceso proceso = gsonUtil.decode(contenido.getText(), Proceso.class);
		System.out.println(consumerName + " processing job: " + proceso);
                simularEjecucion(proceso);
            } else
                System.out.println(consumerName + " Unknown message");
	} catch (JMSException | InterruptedException ex) {			
            Logger.getLogger(ProcesoListener.class.getName()).log(Level.SEVERE, null, ex);
	}
    }
    
    private void simularEjecucion(Proceso proceso) throws InterruptedException {
        proceso.setEstado(EN_EJECUCION);
        System.out.println("TAREA-" + consumerName + " Ejecuta el proceso: " + proceso);
        TimeUnit.SECONDS.sleep(proceso.tiempoEjecucion());
        proceso.setEstado(FINALIZADO);
    }
}
